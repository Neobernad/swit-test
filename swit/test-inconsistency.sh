#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:lib

switOutputDir="swit-output"
inconsistencyOutputdir="inconsistency-example-output"


moveOutputDataTo() {
	targetDir=$1
	mkdir $targetDir
	mv $switOutputDir/* $targetDir
	rmdir $switOutputDir
}

##
#	MAIN
##

echo "Running inconsistency HPC SWIT test with persons-100.xml dataset"
./bin/parallel-swit.sh -f inconsistency-example/persons-100.xml -m data-persons/mappings.xml \
	-o data-persons/persons.owl --format=owl --consistency -t 1
echo ""

moveOutputDataTo $inconsistencyOutputdir

