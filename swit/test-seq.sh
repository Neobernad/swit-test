#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:lib

switOutputDir="swit-output"
orthoxmlOutputDir="orthoxml-output"
imdbOutputDir="imdb-output"
personsOutputDir="persons-output"


moveOutputDataTo() {
	targetDir=$1
	mkdir $targetDir
	mv $switOutputDir/* $targetDir
	rmdir $switOutputDir
}

##
#	MAIN
##

# Source: http://inparanoid.sbc.su.se/download/8.0_current/Orthologs_OrthoXML/E.coli/
echo "Running sequential HPC SWIT test with E.coli files from InParanoid"
echo "Processing: data-orthoxml/E.coli/"
./bin/parallel-swit.sh -f data-orthoxml/E.coli/ -m data-orthoxml/mappingsOrthoXMLOO_3.xml \
-o data-orthoxml/oo.owl -i data-orthoxml/identity.xml --format=owl --consistency -t 1
echo ""
echo ""
moveOutputDataTo $orthoxmlOutputDir

# Source: https://datasets.imdbws.com/
echo "Running sequential HPC SWIT test with IMDB files"
echo "Processing: data-imdb/small-xml/name.basics-small.xml"
./bin/parallel-swit.sh -f data-imdb/small-xml/name.basics-small.xml -m data-imdb/imdb-mappings.xml \
-o data-imdb/movieontology.owl -i data-imdb/identity/name.basics-identity.xml --format=owl -t 1
echo ""

echo "Processing: data-imdb/small-xml/title.akas-small.xml"
./bin/parallel-swit.sh -f data-imdb/small-xml/title.akas-small.xml -m data-imdb/imdb-mappings.xml \
-o data-imdb/movieontology.owl -i data-imdb/identity/title.akas-identity.xml --format=owl -t 1
echo ""

echo "Processing: data-imdb/small-xml/title.basics-small.xml"
./bin/parallel-swit.sh -f data-imdb/small-xml/title.basics-small.xml -m data-imdb/imdb-mappings.xml \
-o data-imdb/movieontology.owl -i data-imdb/identity/title.basics-identity.xml --format=owl -t 1
echo ""

echo "Processing: data-imdb/small-xml/title.crew-small.xml"
./bin/parallel-swit.sh -f data-imdb/small-xml/title.crew-small.xml -m data-imdb/imdb-mappings.xml \
-o data-imdb/movieontology.owl -i data-imdb/identity/title.crew-identity.xml --format=owl -t 1
echo ""

echo "Processing: data-imdb/small-xml/title.episode-small.xml"
./bin/parallel-swit.sh -f data-imdb/small-xml/title.episode-small.xml -m data-imdb/imdb-mappings.xml \
-o data-imdb/movieontology.owl -i data-imdb/identity/title.episode-identity.xml --format=owl -t 1
echo ""

echo "Processing: data-imdb/small-xml/title.principals-small.xml"
./bin/parallel-swit.sh -f data-imdb/small-xml/title.principals-small.xml -m data-imdb/imdb-mappings.xml \
-o data-imdb/movieontology.owl -i data-imdb/identity/title.principals-identity.xml --format=owl -t 1
echo ""

echo "Processing: data-imdb/small-xml/title.ratings-small.xml"
./bin/parallel-swit.sh -f data-imdb/small-xml/title.ratings-small.xml -m data-imdb/imdb-mappings.xml \
-o data-imdb/movieontology.owl -i data-imdb/identity/title.ratings-identity.xml --format=owl --consistency -t 1
echo ""
echo ""

moveOutputDataTo $imdbOutputDir

# Source: https://github.com/datAcron-project/RDF-Gen
echo "Running sequential HPC SWIT test with persons dataset" 
FILES=($(find "data-persons/xml/" -name "*.xml"))

./bin/parallel-swit.sh -f data-persons/xml/ -m data-persons/mappings.xml -o data-persons/persons.owl -i data-persons/identity.xml \
	--format=owl --consistency -t 1
echo ""

moveOutputDataTo $personsOutputDir

echo "Transformations saved in $orthoxmlOutputDir/, $imdbOutputDir/ and $personsOutputDir/"
