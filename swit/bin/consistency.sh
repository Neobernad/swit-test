#!/bin/bash

GNU_PARALLEL=0
REASONER_EXEC="java -jar ./bin/reasoner.jar"


cmdExists () {
    command -v "$1" >/dev/null
}


runHermit() {
	inputDir=$1
	if [ "$GNU_PARALLEL" -eq "0" ]; then
		for filepath in "$inputDir/"*.owl; do
		    echo "Checking $filepath"
		    $REASONER_EXEC -i "$filepath"
		done
	else
		ls -1 $inputDir | grep ".owl" | parallel "echo 'Checking \"$inputDir/{}\"' && $REASONER_EXEC -i $inputDir/{}"
	fi
}

##
#	MAIN
##

if cmdExists "parallel"; then
    echo "Found 'parallel' command, using GNU Parallel version"
    GNU_PARALLEL=1
else
    echo "I did not find 'parallel' command, using sequential version"
fi


for directory in "$@"; do
	if [ ! -d "$directory" ]; then
		echo "Directory '$directory' does not exists, skipping..."
		continue
	fi

	echo "Checking consistency for input: $directory"
	time runHermit "$directory"
done