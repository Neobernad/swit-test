#!/bin/bash

################################################
##											  ##
##				Parallel SWIT 2.0 			  ##
##											  ##
################################################

# Global vars
BIN_HOME="./bin"
FILES=""
FILES_FILE="swit-files"
NUM_FILES=0
MAP_FILE=""
OWL_FILE=""
IDENTITY_FILE=""
COMPLETE=""
JOBS_CORE=1
THREADS=0 # 0 as gnu parallel default thread parameter is j0, and 0 means ALL cores
FORMAT=""
COMPRESSION=""
CONSISTENCY=0 # --consistency flag
HOST="" # www.example.com
HOST_ARG="" # -host www.example.com
PORT=0
PORT_ARG=""
GRAPH_URI_ARG=""


# Functions

clearData() {
	if [[ -f $FILES_FILE ]]; then # If the argument next to -f is a file
	    rm $FILES_FILE
	fi
	if [[ -f "swit.log" ]]; then # If the argument next to -f is a file
	    rm "swit.log"
	fi
}

usage() {
	echo ""
	echo "Usage: $0 -f directory -m mapping_file -o ontology_file [-i identity_file]"
	echo "[-t num_threads] [-j num_jobs_per_core] [-thz] [--complete] [--format=(rdf|owl|ttl|virtuoso)] [--consistency] [--host=host.com] [--port=8890]"
	clearData
}

helpMe() {
	usage
	echo ""
	echo -e "Mandatory parameters"
	echo -e "\t-f: Directory where input data is located"
	echo -e "\t-m: Mapping file"
	echo -e "\t-o: Ontology file domain"
	echo -e "Optional parameters"
	echo -e "\t-j: Number of jobs to run per core (value greater than 0 or lesser than the number of files)"
	echo -e "\t-t: Number of threads to be run (value greater than 0)"
	echo -e "\t-h: Shows this menu"
	echo -e "\t-z: Compress the output in 'zip' (1 zip per input file)"
	echo -e "\t--complete: Complete individuals"
	echo -e "\t--format=: Specifies the format of the output files. It takes the values 'rdf', 'owl' (RDF/OWL), 'ttl' (Turtle) or 'virtuoso' (Virtuoso DB)"
	echo -e "\t--consistency: Check consistency of each output file using HermiT reasoner"
	echo -e "\t--host=: Mandatory if --format=virtuoso. It sets the location of the virtuoso server"
	echo -e "\t--port=: Mandatory if --format=virtuoso. It sets the port for connecting to the virtuoso server"
	echo -e "\t--graph=: A custom graph URI. By default graph URI is the ontology URI"
}

getInputFormat() {
	composition=""
	for (( i = 1; i <= $JOBS_CORE; i++ )); do
		composition+="{$i}"
		next=$((i+1))
		if [[ $next -le $JOBS_CORE ]]; then
			composition+=" "
		fi
	done
	echo $composition # Use "echo", so we can capture the "return" value by calling $(getInputFormat)
}

dumpFiles() {
	composition=""
	for (( i = 0; i < $JOBS_CORE; i++ )); do
		composition+="%s"
		next=$((i+1))
		if [[ $next -lt $JOBS_CORE ]]; then
			composition+=" "
		fi
	done
	printf "$composition\n" "${FILES[@]}" > $FILES_FILE
}

checkMantadoryParams() {

	if [ -z "$FILES" ]; then
		echo "Option -f is mandatory" >&2
		usage
		exit 1
	fi

	if [ -z "$MAP_FILE" ]; then
		echo "Option -m is mandatory" >&2
		usage
		exit 1
	fi

	if [ -z "$OWL_FILE" ]; then
		echo "Option -o is mandatory" >&2
		usage
		exit 1
	fi

	## If format == virtuoso, check if HOST and PORT are set
	if [ "$FORMAT" = "--format virtuoso" ]; then 
		if [ ! -z "$COMPRESSION" ]; then
			echo "Option -z is invalid for format 'virtuoso'. Cannot send compressed zip into virtuoso " >&2
			usage
			exit 1
		fi
		if [ -z "$HOST" ]; then
			echo "Option --host is mandatory for format 'virtuoso'" >&2
			usage
			exit 1
		fi
		if [ $PORT -eq 0 ]; then
			echo "Option --port is mandatory for format 'virtuoso'" >&2
			usage
			exit 1
		fi
		# Check connection
		state=`nmap -p $PORT $HOST | grep "$PORT" | grep open`
		if [ -z "$state" ]; then
		  echo "Connection to $HOST on port $PORT has failed"
		  exit 1
		else
		  echo "Connection to $HOST on port $PORT was successful"
		fi
	fi

	# Check if number of jobs per core is lesser than number of files to process
	ratio=$((NUM_FILES / $JOBS_CORE))
	if [[ $ratio -eq 0 ]]; then
		echo "The number of jobs ($JOBS_CORE) exceeds the number of files to process ($NUM_FILES)" >&2
		usage
		exit 1
	fi
}

parseArgs () {
	if [ "$#" -eq 0 ]; then
    	echo "Illegal number of parameters ($#)"
    	helpMe
    	exit 1
	fi
	# :f: :m: :o: :i: :t: :j: :z -:(-format, -complete)
	while getopts ":f::m::o::i::t::j::zh-:" opt; do
		case $opt in
		f) # Input directory
			if [[ -d $OPTARG ]]; then # If the argument next to -f is a directory
				FILES=( $(find $OPTARG -maxdepth 2 -type f) )
				NUM_FILES=${#FILES[@]}
				if [ $NUM_FILES -eq 0 ]; then
					echo "No files found in $OPTARG, please specify a directory"
					usage
					exit 1
				fi
			else
				FILES=( $OPTARG )
			    NUM_FILES=1
			fi
			;;
		m) # mapping file
			if [[ -f $OPTARG ]]; then # If the argument next to -f is a file
			    MAP_FILE=$OPTARG
			else
				echo "Invalid -$opt option: $OPTARG" >&2
				usage
				exit 1
			fi
			;;
		o) # ontology file
			if [[ -f $OPTARG ]]; then # If the argument next to -f is a file
			    OWL_FILE=$OPTARG
			else
				echo "Invalid -$opt option: $OPTARG" >&2
				usage
				exit 1
			fi
			;;
		i) # identity file
			if [[ -f $OPTARG ]]; then
			    IDENTITY_FILE="-i $OPTARG"
			else
				echo "Invalid -$opt option: $OPTARG" >&2
				usage
				exit 1
			fi
			;;
		t) # threads
			if [[ $OPTARG -gt 0 ]]; then
			    THREADS=$OPTARG
			else
				echo "Invalid -$opt option: $OPTARG. It must be greater than 0." >&2
				usage
				exit 1
			fi
			;;
		j) # jobs
			if [[ $OPTARG -gt 0 ]]; then
			    JOBS_CORE=$OPTARG
			else
				echo "Invalid -$opt option: $OPTARG. It must be greater than 0." >&2
				usage
				exit 1
			fi
			;;
		z)
			COMPRESSION="-z"
			;;
		h)
			helpMe
			exit 1
			;;
		-) # For --param or --param=X
            case "${OPTARG}" in
                format=*)
					val=${OPTARG#*=} # value after "="
                    opt=${OPTARG%=$val} # parameter --format
                    if [ "$val" = "owl" ]; then
				  		FORMAT="--${opt} owl"
					elif [ "$val" = "ttl" ]; then
						FORMAT="--${opt} ttl"
					elif [ "$val" = "virtuoso" ]; then
						FORMAT="--${opt} virtuoso"
					elif [ "$val" = "rdf" ]; then
						FORMAT="--${opt} rdf"
					else
						echo "Unknown value '${val}' for option '--${opt}'" >&2
                        usage
						exit 1
					fi
                    ;;
                format)
                    echo "Error: option --${OPTARG} must have a value \"owl\", \"ttl\" or \"virtuoso\"" >&2
                    usage
					exit 1
                    ;;
                host=*)
					val=${OPTARG#*=} # value after "="
                    opt=${OPTARG%=$val} # parameter --format
                    regex='^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$'
					if [[ $val =~ $regex ]]; then 
					    HOST=$val
					    HOST_ARG="--host $val"
					else
					    echo "The URL $val does not follow an URL pattern. Please, specify a valid URL" >&2
					    exit 1
					fi
                    ;;
                host)
                    echo "Error: option --${OPTARG} must have an URL" >&2
                    usage
					exit 1
                    ;;
             	port=*)
					val=${OPTARG#*=} # value after "="
                    opt=${OPTARG%=$val} # parameter --format
                    regex='^[0-9]+$'
					if [[ $val =~ $regex ]] # $ is $val a number?
					then
					    if [[ $val -gt 0 ]]; then # $ is $val greater than 0
					    	PORT=$val
				    	    PORT_ARG="--port $val"
					    else
					    	echo "Invalid port number" >&2
					    	exit 1
					    fi
					else
					    echo "The port '$val' is not a number" >&2
					    exit 1
					fi
                    ;;
                port)
                    echo "Error: option --${OPTARG} must have an port number" >&2
                    usage
					exit 1
                    ;;
                complete)
                    COMPLETE="--complete"
                    ;;
                consistency)
					CONSISTENCY=1
					;;
                graph=*)
					val=${OPTARG#*=} # value after "="
                    opt=${OPTARG%=$val} # parameter --graph
                    GRAPH_URI_ARG="--${opt} $val"
                    ;;
                graph)
                    echo "Error: option --${OPTARG} must have an graph URI" >&2
                    usage
					exit 1
                    ;;   
                *)
                    if [ "$OPTERR" = 1 ] && [ "${optspec:0:1}" != ":" ]; then
                        echo "Unknown option --${OPTARG}" >&2
                        usage
						exit 1
                    fi
                    ;;
            esac;;
		\?)
			echo "Invalid option: -$OPTARG" >&2
			usage
			exit 1
			;;
		:)
			echo "Option -$OPTARG requires an argument." >&2
			usage
			exit 1
			;;
		esac
	done


	# Check mandatory requisites to run SWIT
	checkMantadoryParams

	# Everything ok? then dump the file paths to a file
	dumpFiles
	
}

##########
## Main ##
##########

parseArgs $@

echo -e "\nNumber of files to process: $NUM_FILES"

time cat $FILES_FILE | parallel --colsep ' ' "-j$THREADS" "$BIN_HOME/swit" -f "$(getInputFormat)" -m "$MAP_FILE" -o "$OWL_FILE" \
"$IDENTITY_FILE" $COMPLETE $FORMAT $HOST_ARG $PORT_ARG $COMPRESSION $GRAPH_URI_ARG | grep -e "Individuals processed:" #'>' /dev/null '2>&1'

if [ $CONSISTENCY -eq 1 ]; then	
	"$BIN_HOME/consistency.sh" "swit-output"
fi

clearData
