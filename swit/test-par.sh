#!/bin/bash

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:lib

switOutputDir="swit-output"
orthoxmlOutputParDir="orthoxml-par-output"
imdbOutputParDir="imdb-par-output"
personsOutputParDir="persons-par-output"
personsSplitOutputParDir="persons-par-split-output"


moveOutputDataTo() {
	targetDir=$1
	mkdir $targetDir &&> /dev/null
	mv $switOutputDir/* $targetDir
	rmdir $switOutputDir
	echo "Data saved in $targetDir"
}

##
#	MAIN
##

if ! type "parallel" > /dev/null 2>&1; then
  echo "Parallel command not installed."
  echo "To install the library run: \`(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - pi.dk/3) | bash\`"
  exit 1
fi

echo "Running parallel SWIT test with E.coli files from InParanoid" # http://inparanoid.sbc.su.se/download/8.0_current/Orthologs_OrthoXML/E.coli/

dir="data-orthoxml/E.coli/"
files=( $(find $dir -maxdepth 1 -type f) )
num_files=${#files[@]}
echo -e "\nNumber of files to process: $num_files\n"
./bin/parallel-swit.sh -f $dir -m data-orthoxml/mappingsOrthoXMLOO_3.xml -o data-orthoxml/oo.owl \
	-i data-orthoxml/identity.xml --format=owl --consistency
echo ""


moveOutputDataTo $orthoxmlOutputParDir

echo "Running parallel SWIT test with imdb test datasets"
dir="data-imdb/small-xml/"
files=( $(find $dir -maxdepth 1 -type f) )
num_files=${#files[@]}
echo -e "\nNumber of files to process: $num_files\n"
./bin/parallel-swit.sh -f $dir -m data-imdb/imdb-mappings.xml -o data-imdb/movieontology.owl \
	--format=owl --consistency
echo ""

moveOutputDataTo $imdbOutputParDir

echo "Running parallel SWIT test with person dataset"
dir="data-persons/xml/"
files=( $(find $dir -maxdepth 1 -type f) )
num_files=${#files[@]}
echo -e "\nNumber of files to process: $num_files\n"
./bin/parallel-swit.sh -f $dir -m data-persons/mappings.xml -o data-persons/persons.owl \
	--format=owl --consistency
echo ""

moveOutputDataTo $personsOutputParDir

echo "Running parallel SWIT test with person dataset (each dataset is split into 30 files to exploit paralelism)"
dir="data-persons/xml-split/"
files=( $(find $dir -maxdepth 1 -type f) )
num_files=${#files[@]}
echo -e "\nNumber of files to process: $num_files\n"
./bin/parallel-swit.sh -f $dir -m data-persons/mappings.xml -o data-persons/persons.owl \
	--format=owl --consistency
echo ""

moveOutputDataTo $personsSplitOutputParDir
