# README #

SWIT uses Semantic Web technologies to integrate data from heterogenous sources.
It processes data from XML extracts based on clinical archetypes or XML data based on XSD schemas and maps it with OWL ontologies generating RDF and OWL repositories.

## What is this repository for? ##

* This is a demo of the optimized version of SWIT. The original version can be found [here](http://sele.inf.um.es/swit/documentation.html)
* Version 2.0

## How do I get set up? ##

The current executable is compiled for Linux based OS. It was tested on CentOS 7 x64 and Ubuntu 14.04 and Ubuntu 16.04.
The following libraries are required in order to run SWIT:

* XML processing: [libxml](http://xmlsoft.org/)

* Compression management: [libzip](https://libzip.org/)

* Send/receive data from the internet: [libcurl](https://curl.haxx.se/libcurl/)

* Argument parsing: [libboost::program_options](https://www.boost.org/doc/libs/1_58_0/doc/html/program_options.html)

* Parallel execution: [GNU Parallel](https://www.gnu.org/software/parallel/) tool

### Library installation ###
If the shared libraries contained in the *lib* folder will not work. You can download the missing libraries in the following steps. **Note**: since the executable is precompiled, the library version must match in order to get it to work.

* libxml2
```
# CentOS:
~$ sudo yum install libxml2
~$ sudo yum install libxml2-devel
# Ubuntu:
~$ sudo apt-get install libxml2
~$ sudo apt-get install libxml2-devl
```

* zlib
```
# Centos:
~$ sudo yum install zlib-devel
# Ubuntu:
~$ sudo apt-get install zlib1g-dev
```

* libcurl
```
# Centos:
~$ sudo yum install curl-devel
# Ubuntu:
~$ sudo apt-get install libcurl-dev
```

* program-options (libboost)
```
# Centos:
~$ sudo yum install boost-devel
# Ubuntu:
~$ sudo apt-get install libboost-program-options-dev
```

* GNU Parallel
```
~$ `(wget -O - pi.dk/3 || curl pi.dk/3/ || fetch -o - pi.dk/3) | bash`
```

## Test run ##

There are two different test scripts in the repository, one for sequantial execution and another one for a parallel run.
The output data will be stored in a "swit-output" directory.

- [`run.sh`](swit/run.sh): It will run a sequential SWIT execution for the *E.coli* files from [InParanoid](http://inparanoid.sbc.su.se/download/8.0_current/Orthologs_OrthoXML/), the [IMDB](https://datasets.imdbws.com/) test datasets and [persons](https://github.com/datAcron-project/RDF-Gen) files.
- [`run-parallel.sh`](swit/run-parallel.sh): Parallel SWIT execution of the *E.coli*, test IMDB and *persons* files.
